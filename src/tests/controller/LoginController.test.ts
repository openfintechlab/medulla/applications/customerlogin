import LoginController from "../../mapping/LoginController";


describe("Logincontroller test stub", ()=>{
    it('Parse Accounts request message using Accounts Controller', ()=> {
        let loginController:LoginController = new LoginController(JSON.parse('{"user":{"id":"abc@mail.com","secret":[{"type":"pass","text":"kmMj7s6U4JsJhZn"}]}}'));
        expect(loginController.userObjBO.user.id).toBe('abc@mail.com');
    });

    it('Generate parsing error', ()=> {
        try{
            let loginController:LoginController = new LoginController(JSON.parse('"user":{"id":"abc@mail.com","secret":[{"type":"pass","text":"kmMj7s6U4JsJhZn"}]}'));
            expect(loginController.userObjBO.user.id).not.toBe('abc@mail.com');
        }catch(error){
            expect(error).toBeDefined();
        }        
    });

    it('Generate Siumulated 0000 simulated response', () => {
        let loginController:LoginController = new LoginController(JSON.parse('{"user":{"id":"abc@mail.com","secret":[{"type":"pass","text":"kmMj7s6U4JsJhZn"}]}}'));
        loginController.logIn().then((response:String) => {
            expect(JSON.parse(response.toString()).metadata.status).toBe('0000');
        });
    });

    it('Generate Siumulated 8404 simulated response', () => {
        let loginController:LoginController = new LoginController(JSON.parse('{"user":{"id":"abc@mail.com1","secret":[{"type":"pass","text":"kmMj7s6U4JsJhZn"}]}}'));
        loginController.logIn().then((response:String) => {
            expect(JSON.parse(response.toString()).metadata.status).not.toBe('0000');
        }).catch((error:String) => {
            expect(JSON.parse(error.toString()).metadata.status).toBe('8404');
        });
    });
});