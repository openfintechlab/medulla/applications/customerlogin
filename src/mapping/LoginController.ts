import logger       from "../utils/Logger";
import IUser        from "./bussObj/IUser";
import usersJSON    from "./simulator/LoginRepo.json";
import {PostRespBusinessObjects}     from "./bussObj/Response";
import { rejects } from "assert";


export default class LoginController{
    private userObj!:IUser;

    constructor(reqBody:string){
        this.parse(JSON.stringify(reqBody));
    }

    /**
     * Parse the JSON object as per IReqThroughAccounts interface
     * @param strBody JSON object wrapped in string
     */
    public parse(strBody: string){   
        logger.debug('Parsing message '+strBody);                
        this.userObj = JSON.parse(strBody);                        
    }
    
    public logIn(): Promise<String>{
        let respBo:PostRespBusinessObjects.PostingResponse = new PostRespBusinessObjects.PostingResponse();
        return new Promise<String>((resolve:any, reject:any) => {
            for(let index in usersJSON.users){
                if(usersJSON.users[index].user.id === this.userObj.user.id && 
                    usersJSON.users[index].user.pass === this.userObj.user.secret[0].text && 
                    this.userObj.user.secret[0].type === 'pass' &&
                    usersJSON.users[index].user.status === 'active'){
                        resolve(respBo.generateBasicResponse("0000","Success!"));
                }
            }
            reject(respBo.generateBasicResponse("8404","Error while logging in"));
        });
    }

    get userObjBO(): IUser{
        return this.userObj;
    }
}