/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import logger  from "../utils/Logger";
import LoginController from "../mapping/LoginController";

const router: any = express.Router();

/**
 * Functional route definition
 * MD-CL-001: Validates the customer and provides token accordingly
 */
router.post('/login',(req:any, res:any) => {
    logger.debug("Registiration request for db-card: " + req.body);
    let user:LoginController = new LoginController(req.body);
    user.logIn().then((result:String) => {
        res.set("Content-Type","application/json; charset=utf-8");
        res.send(result);
    }).catch((error:any)=> {
        res.set("Content-Type","application/json; charset=utf-8");
        res.status(500);
        res.send(error);
    });
});

/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);    
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;